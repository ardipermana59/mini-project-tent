<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Auth\LoginRequest;
use Auth;

class LoginController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(LoginRequest $request)
    {

        if(!$token = auth()->attempt($request->only('no_hp','password'))){
            return response()->json(['error' => 'password atau no hp mungkin salah'], 401);
        }
        $token = $token;
        return response()->json([
            'message' => 'anda berhasil login',
            'token' => $token,
            'data' => Auth::user(),
        ]); 
    }
}
