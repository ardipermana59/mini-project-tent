<?php

namespace App\Http\Controllers\Socialite;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
// use Laravel\Socialite\Facades\Socialite;
use App\Models\SocialAccount;
use App\Models\User;
use Auth;
use JWTAuth;
use Socialite;

class SocialiteController extends Controller
{
    public function redirectToProvider($provider)
    {

    	return Socialite::driver($provider)->redirect();
    }

    public function handleProviderCallback($provider)
    {
    	try {
    		$user = Socialite::driver($provider)->stateless()->user();
    	} catch (\Exception $e) {
    		return redirect('/');
    	}

    	$authUser = $this->findOrCreateUser($user, $provider);

    	Auth::login($authUser, true);

    	return response()->json(
            [
            'name' => $user->name,
            'token' => $user->token
            ],
            200);
    }

    public function findOrCreateUser($socialUser, $provider)
    {
    	$socialAccount = SocialAccount::where('provider_id', $socialUser->getId())->where('provider_name', $provider)->first();

    	if($socialAccount){
    		$user = $socialAccount->user;
    	} else {
    		$user = User::where('email', $socialUser->getEmail())->first();

    		if(! $user){
    			$user = User::create([
    				'name' => $socialUser->getName(),
    				'email' => $socialUser->getEmail(),
    			]);
    		}

    		$user->socialAccounts()->create([
    			'provider_id' => $socialUser->getId(),
    			'provider_name' => $provider,
    		]);

    	}
        return $user;
    }
}
