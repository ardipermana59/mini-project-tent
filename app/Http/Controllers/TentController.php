<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\Tent\TentRequest;
use App\Http\Resources\TentResource;
use App\Models\Tent;

class TentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Tent $tent)
    {
        $tent = Tent::get();
        return TentResource::collection($tent);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TentRequest $request)
    {
        $img = $this->uploadImg(request('img'));

         $tent = Tent::create([
                'jenis' => request('jenis'),
                'max' => request('max'),
                'bahan_tenda' => request('bahan_tenda'),
                'bahan_alas' => request('bahan_alas'),
                'bahan_frame' => request('bahan_frame'),
                'warna' => request('warna'),
                'berat' => request('berat'),
                'harga' => request('harga'),
                'unit' => request('unit'),
                'img' => env('APP_URL') . '/' . $img,
        ]);

        return response()->json(
        [
            'message' => 'berhasil input data tenda',
            'data' => $tent,
        ]
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Tent $tent)
    {
        return new TentResource($tent);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Tent $tent)
    {
        $img = $this->uploadImg(request('img'));
        $tent->update($this->tentStore($img));

        return new TentResource($tent);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Tent $tent)
    {
        $tent->delete();

        return response()->json('Deleted', 200);
    }

    public function uploadImg($img = null)
    {
        if( $img == null ){
            return null;
        }
        $file = $img;
        
        $imgName = time()."_".$file->getClientOriginalName();
        $file->move('thumbsnail/tent',$imgName);
        return $imgName;
    }

    public function tentStore($img)
    {
        return [
                'jenis' => request('jenis'),
                'max' => request('max'),
                'bahan_tenda' => request('bahan_tenda'),
                'bahan_alas' => request('bahan_alas'),
                'bahan_frame' => request('bahan_frame'),
                'warna' => request('warna'),
                'berat' => request('berat'),
                'harga' => request('harga'),
                'unit' => request('unit'),
                'img' => env('APP_URL') . '/' . $img,
        ];
    }
}
