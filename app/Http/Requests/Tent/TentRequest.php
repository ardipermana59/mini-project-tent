<?php

namespace App\Http\Requests\Tent;

use Illuminate\Foundation\Http\FormRequest;

class TentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'jenis' => ['required'],
            'max' => ['required'],
            'bahan_tenda' => ['required'],
            'bahan_alas' => ['required'],
            'bahan_frame' => ['required'],
            'warna' => ['required'],
            'berat' => ['required'],
            'harga' => ['required'],
            'unit' => ['required'],
            'img' => ['required','file','image','mimes:jpeg,jpg,png','max:2120'],
        ];
    }
}
