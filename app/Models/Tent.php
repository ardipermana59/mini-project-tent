<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tent extends Model
{
    protected $fillable = ['jenis','max','bahan_tenda','bahan_alas','bahan_frame','warna','berat','harga','unit','img'];

}
