<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tents', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('jenis')->nullable();
            $table->integer('max')->nullable();
            $table->string('bahan_tenda')->nullable();
            $table->string('bahan_alas')->nullable();
            $table->string('bahan_frame')->nullable();
            $table->string('warna')->nullable();
            $table->string('berat')->nullable();
            $table->integer('harga')->nullable();
            $table->integer('unit')->nullable();
            $table->string('img')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tents');
    }
}
