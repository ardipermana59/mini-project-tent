<?php

use Illuminate\Database\Seeder;
use App\Models\Tent;

class TentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$tents = [
    		[
    			'jenis' => 'Dom',
                'max' => '2',
                'bahan_tenda' => 'Parasit Tahan Air',
                'bahan_alas' => 'Terpal',
                'bahan_frame' => 'Fiber',
                'warna' => 'Biru-Kuning',
                'berat' => '1600gr',
                'harga' => '200000',
                'unit' => '11',
                'img' => 'dom-biru-kuning.jpg',
    		],
    		[
    			'jenis' => 'Dom',
                'max' => '7',
                'bahan_tenda' => 'Parasit Tahan Air',
                'bahan_alas' => 'Terpal',
                'bahan_frame' => 'Fiber',
                'warna' => 'Biru',
                'berat' => '2200gr',
                'harga' => '350000',
                'unit' => '20',
                'img' => 'dom-biru.jpg',
    		],
    		[
    			'jenis' => 'Dom',
                'max' => '7',
                'bahan_tenda' => 'Parasit Tahan Air',
                'bahan_alas' => 'Terpal',
                'bahan_frame' => 'Fiber',
                'warna' => 'kuning',
                'berat' => '1600gr',
                'harga' => '550000',
                'unit' => '7',
                'img' => 'dom-kuning.jpg',
    		],
    	];
    	foreach($tents as $tent){
    		Tent::create([
                'jenis' => $tent['jenis'], 
                'max' => $tent['max'], 
                'bahan_tenda' => $tent['bahan_tenda'], 
                'bahan_alas' => $tent['bahan_alas'], 
                'bahan_frame' => $tent['bahan_frame'],  
                'warna' => $tent['warna'], 
                'berat' => $tent['berat'], 
                'harga' => $tent['harga'], 
                'unit' => $tent['unit'], 
                'img' =>  env('APP_URL') . 'thumbnails/tent/' . $tent['img'],
        ]);
    	}
       
    }
}
