<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <style type="text/css">
    	@media screen and (max-width: 500px){
    		.nav {
    			display: none;
    		}
    	}
    </style>
    <title>Hello, world!</title>
  </head>
  <body>

	<div class="nav float-right">
		<ul class="" style="list-style: none;">
	      <li class="d-inline ">
	        <a class="nav-link badge badge-primary p-3" href="{{url('api/auth/facebook')}}">Login Facebook</a>
	      </li>
	      <li class="d-inline ">
	        <a class="nav-link badge badge-warning p-3" href="{{url('api/auth/twitter')}}">Login Twitter</a>
	      </li>
	    </ul>
	</div>
    <main class="container">
		<div class="row">
			<div class="col">
				<h1>Get All The Tents</h1>
				<p>Mendapatkan semua data tenda</p>
				<ul>
					<li class="m-1"><b>URL</b></li>
					<span>/api/tents</span>
					<li class="m-1"><b>Method</b></li>
					<span class="text-danger">GET</span>
				</ul>
			</div>
		</div>
		<div class="row">
			<div class="col">
				<h1>Get a Tent</h1>
				<p>Mendapatkan 1 data tenda</p>
				<ul>
					<li class="m-1"><b>URL</b></li>
					<span>/api/tents/<span class="text-danger">id</span></span>
					<li class="m-1"><b>Method</b></li>
					<span class="text-danger">GET</span>
				</ul>
			</div>
		</div>

  </body>
</html>