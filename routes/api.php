<?php
use Illuminate\Support\Facades\Route;

Route::namespace('Auth')->group(function () {
	Route::post('register','RegisterController');
	Route::post('login','LoginController');
	Route::post('logout','LogoutController');
});
	

Route::middleware('auth:api','IsAdmin')->group(function(){
	Route::post('tent/add','TentController@store')->name('tent.add');
	Route::patch('tent/update/{tent}', 'TentController@update')->name('tent.update');
	Route::delete('tent/delete/{tent}', 'TentController@destroy')->name('tent.destroy');
});
Route::get('tent/{tent}', 'TentController@show')->name('tent.show');
Route::get('tents', 'TentController@index')->name('tent.index');


// Yang ini belum jalan. Yang baru jalan di route web.php

Route::group(['middleware' => ['web']], function () {
    Route::get('auth/{provider}','Socialite\SocialiteController@redirectToProvider');
Route::get('auth/{provider}/callback','Socialite\SocialiteController@handleProviderCallback');
});